import autocomplete from 'jquery-ui/ui/widgets/autocomplete';

import './modules/windows/index';
import './modules/openable-box/index';
import './modules/popup/index';
import './modules/scroll/index';
import './modules/subscribe/index';
import './modules/subscribe/autocomplete';


import '../sass/app.sass';
