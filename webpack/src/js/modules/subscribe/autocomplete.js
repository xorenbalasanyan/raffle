$(".js-subscribe-name").autocomplete({
    source: function( request, response ) {
        $.ajax({
            url: $(".js-subscribe-name").data('autocomplete'),
            dataType: "json",
            data: {
                q: request.term
            },
            success: function( data ) {
                response(data.list);
            }
        });
    },
});

$(".js-subscribe-office").autocomplete({
    source: function( request, response ) {
        $.ajax({
            url: $(".js-subscribe-office").data('autocomplete'),
            dataType: "json",
            data: {
                q: request.term
            },
            success: function( data ) {
                response(data.list);
            }
        });
    },
});

$(".js-subscribe-department").autocomplete({
    source: function( request, response ) {
        $.ajax({
            url: $(".js-subscribe-department").data('autocomplete'),
            dataType: "json",
            data: {
                q: request.term
            },
            success: function( data ) {
                response(data.list);
            }
        });
    },
});
