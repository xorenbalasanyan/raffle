$('.js-subscribe-form').on('submit', function(event) {
    event.preventDefault();
    var form = $(this);

    $('.js-popup-subscribe').fadeOut('slow', function(){
        $('.js-popup-message-text').text('Обрабатывается...');
        $('.js-popup-message').fadeIn('slow', function(){
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType: 'json',
                data: form.serialize(),
                async: false,
                success: function(data){
                    if (data.success) {
                        $('.js-popup-message').fadeOut('slow', function(){
                            $('.js-popup-thanks').fadeIn('slow');
                        });
                    }
                    else {
                        if ($('.js-popup-message').css('display') != 'none') {
                            $('.js-popup-message-text').text(data.message);
                        }
                    }
                }
            });
        });
    });
    return false;
});

if (window.location.hash == '#subscribed') {
    $('.js-popup-message-text').text('Регистрация прошла успешно!');
    $('.js-popup-message').fadeIn('slow');
}
