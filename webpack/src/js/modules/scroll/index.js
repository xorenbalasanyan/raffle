$(".js-scroll-to").click(function (e){
    e.preventDefault();
    var element = $(this).attr('href');
    $('html, body').animate({
        scrollTop: $(element).offset().top
    }, 700);
});
