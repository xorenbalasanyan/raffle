$('.js-openable-box-btn').click(function(event) {
    var box  = $(this).closest('.js-openable-box'),
        text = box.find('.js-openable-box-text');

    if (text.css('display') != 'none') {
        $(this).removeClass('active');
        text.slideUp('fast');
    }
    else {
        $(this).addClass('active');
        text.slideDown('fast');
    }
});

$('.js-gift-to-give-btn').click(function(event) {
    var box  = $('#js-gift-to-give'),
        text = box.find('.js-openable-box-text'),
        link = box.find('.js-openable-box-btn');

    if (text.css('display') == 'none') {
        link.addClass('active');
        text.slideDown('fast');
    }
});
