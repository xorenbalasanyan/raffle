$('.js-popup-open').click(function(event) {
    event.preventDefault();
    var popup = $(this).data('popup');
    $('.'+popup).fadeIn('slow');
    $('body').addClass('main-no-scroll');
});

$('.js-popup-close').click(function(event) {
    event.preventDefault();
    var popup = $(this).closest('.js-popup');
    popup.fadeOut('slow');
    $('body').removeClass('main-no-scroll');
});
