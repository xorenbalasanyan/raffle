from django.urls import path
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(template_name="frontend/accounts/login.html.j2"), name='accounts_login'),
    path('logout/', auth_views.LogoutView.as_view(), name='accounts_logout'),
    path('password_reset/', auth_views.PasswordResetView.as_view(template_name="frontend/accounts/password_reset_form.html.j2", email_template_name = 'frontend/accounts/password_reset_email.html.j2'), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name="frontend/accounts/password_reset_done.html.j2"), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name="frontend/accounts/password_reset_confirm.html.j2"), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name="frontend/accounts/password_reset_complete.html.j2"), name='password_reset_complete'),
]
