from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib.auth.models import User

class CreationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(CreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['password2'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['username'].widget.attrs = {
            'class': 'form-control',
        }

class ChangeForm(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(ChangeForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['first_name'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['last_name'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['email'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['date_joined'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['is_superuser'].widget.attrs = {
            'class': 'custom-control-input',
        }
        self.fields['is_staff'].widget.attrs = {
            'class': 'custom-control-input',
        }
        self.fields['is_active'].widget.attrs = {
            'class': 'custom-control-input',
        }

class ClientsPasswordChangeForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(ClientsPasswordChangeForm, self).__init__(*args, **kwargs)
        self.fields['old_password'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['new_password1'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['new_password2'].widget.attrs = {
            'class': 'form-control',
        }
