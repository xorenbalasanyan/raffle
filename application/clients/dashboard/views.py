from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


from django.contrib.auth.models import User
from clients.dashboard.forms import CreationForm, ChangeForm, ClientsPasswordChangeForm
from main.components import SuperuserRequiredComponent


class ClientsList(SuperuserRequiredComponent, ListView):
    template_name = 'dashboard/clients/list.html.j2'
    model = User
    paginate_by = 30
    ordering = ['-id']

class ClientsCreate(SuperuserRequiredComponent, CreateView):
    form_class = CreationForm
    template_name = 'dashboard/clients/add.html.j2'
    model = User
    success_url = reverse_lazy('clients_list')

class ClientsUpdate(SuperuserRequiredComponent, UpdateView):
    form_class = ChangeForm
    template_name = 'dashboard/clients/form.html.j2'
    model = User
    success_url = reverse_lazy('clients_list')

class ClientsUpdatePassword(SuperuserRequiredComponent, UpdateView):
    form_class = ClientsPasswordChangeForm
    template_name = 'dashboard/clients/form_password.html.j2'
    model = User
    success_url = reverse_lazy('clients_list')

    def get_object(self, queryset=None):
        return self.request.user

    def get_form_kwargs(self):
        kwargs = super(ClientsUpdatePassword, self).get_form_kwargs()
        kwargs['user'] = kwargs.pop('instance')
        return kwargs

class ClientsDelete(SuperuserRequiredComponent, DeleteView):
    template_name = 'dashboard/common/delete.html.j2'
    model = User
    success_url = reverse_lazy('clients_list')
