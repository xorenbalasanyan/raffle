from django.urls import path
from . import views

urlpatterns = [
    path('', views.ClientsList.as_view(), name='clients_list'),
    path('new/', views.ClientsCreate.as_view(), name='clients_new'),
    path('edit/<int:pk>/', views.ClientsUpdate.as_view(), name='clients_edit'),
    path('password/', views.ClientsUpdatePassword.as_view(), name='clients_edit_password'),
    path('delete/<int:pk>/', views.ClientsDelete.as_view(), name='clients_delete'),
]
