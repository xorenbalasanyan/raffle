from django.urls import path
from . import views

urlpatterns = [
    path('', views.CouplesList.as_view(), name='couples_list'),
    path('set/', views.couplesSet, name='couples_set'),
]
