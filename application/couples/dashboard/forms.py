from django import forms
from departments.models import Departments
from offices.models import Offices

class CouplesModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.title

class CouplesForm(forms.Form):
    department = CouplesModelChoiceField(required=True, label='Департамент', queryset=Departments.objects.all())
    office     = CouplesModelChoiceField(required=True, label='Офис', queryset=Offices.objects.all())

    def __init__(self, *args, **kwargs):
        super(CouplesForm, self).__init__(*args, **kwargs)
        self.fields['department'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['office'].widget.attrs = {
            'class': 'form-control'
        }
