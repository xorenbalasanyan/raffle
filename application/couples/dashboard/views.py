from django.views.generic import ListView

from couples.models import Couples
from main.components import SuperuserRequiredComponent
from couples.dashboard.forms import CouplesForm

from django.contrib.auth.decorators import user_passes_test

from django.urls import reverse_lazy
from django.http import HttpResponseRedirect

from employees.models import Employees
import random

from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.conf import settings
import time

def getGiver(user, givers):
    updatedGivers = givers.copy()
    if user in updatedGivers:
        updatedGivers.remove(user)

    return random.choice(updatedGivers)

@user_passes_test(lambda u: u.is_superuser)
def couplesSet(request):
    if request.method == 'POST':
        form = CouplesForm(request.POST)
        if form.is_valid():
            data       = form.cleaned_data
            department = data.get('department')
            office     = data.get('office')
            employees  = Employees.objects.filter(department=department, office=office, status=Employees.STATUS_REGISTERED).values('id', 'giver')
            list       = []

            for employee in employees:
                list.append(employee['id'])

            givers = list.copy()
            takers = list.copy()

            for employee in employees:
                if employee['giver']:
                    if employee['giver'] in givers:
                        givers.remove(employee['giver'])
                    if employee['id'] in takers:
                        takers.remove(employee['id'])

            for user in list:
                if user in takers:
                    giver = getGiver(user, givers)
                    takers.remove(user)
                    givers.remove(giver)

                    taker       = Employees.objects.get(id=user)
                    sender      = Employees.objects.get(id=giver)
                    couple      = Couples.objects.create(sender=sender, taker=taker)
                    taker.giver = sender
                    taker.save()
                    sendMail(sender, taker)
                    time.sleep(1)

    return HttpResponseRedirect(reverse_lazy('couples_list'))

def sendMail(sender, taker):
    msg = EmailMessage(
        'Secret Santa',
        get_template('mail/couple.html.j2').render({
            'taker': taker,
        }),
        to=[sender.email],
        from_email=settings.EMAIL_HOST_USER
    )
    msg.content_subtype = 'html'

    isSend = True
    try:
        msg.send()
    except Exception as e:
        print('There was an error sending an email: ', e)
        isSend = False

    return isSend

class CouplesList(SuperuserRequiredComponent, ListView):
    template_name = 'dashboard/couples/list.html.j2'
    model = Couples
    paginate_by = 1000
    ordering = ['-id']

    def get_context_data(self, **kwargs):
        ctx = super(CouplesList, self).get_context_data(**kwargs)
        ctx['couples_form'] = CouplesForm
        return ctx
