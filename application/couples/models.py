from django.db import models
from employees.models import Employees

class Couples(models.Model):

    sender = models.ForeignKey(Employees, default=None, null=True, on_delete=models.CASCADE, blank=True, related_name='sender_employee')
    taker  = models.ForeignKey(Employees, default=None, null=True, on_delete=models.CASCADE, blank=True, related_name='taker_employee')

    class Meta:
        db_table = "t_couples"
