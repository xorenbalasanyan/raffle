from departments.models import Departments
from django.http import JsonResponse

def autocomplete(request):
    list = []
    departments = Departments.objects.filter(title__icontains=request.GET.get('q')).all()
    for department in departments:
        list.append(department.title)

    return JsonResponse({'list': list})
