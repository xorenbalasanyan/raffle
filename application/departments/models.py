from django.db import models

class Departments(models.Model):

    title = models.CharField(max_length=255, default=None, null=True, blank=True)

    class Meta:
        db_table = "t_departments"
