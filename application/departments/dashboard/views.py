from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from departments.models import Departments
from departments.dashboard.forms import CreateForm
from main.components import SuperuserRequiredComponent

class DepartmentsList(SuperuserRequiredComponent, ListView):
    template_name = 'dashboard/departments/list.html.j2'
    model = Departments
    paginate_by = 30
    ordering = ['-id']

class DepartmentsCreate(SuperuserRequiredComponent, CreateView):
    form_class = CreateForm
    template_name = 'dashboard/departments/form.html.j2'
    model = Departments
    success_url = reverse_lazy('departments_list')

class DepartmentsUpdate(SuperuserRequiredComponent, UpdateView):
    form_class = CreateForm
    template_name = 'dashboard/departments/form.html.j2'
    model = Departments
    success_url = reverse_lazy('departments_list')

class DepartmentsDelete(SuperuserRequiredComponent, DeleteView):
    template_name = 'dashboard/common/delete.html.j2'
    model = Departments
    success_url = reverse_lazy('departments_list')
