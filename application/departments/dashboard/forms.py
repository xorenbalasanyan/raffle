from departments.models import Departments
from django import forms

class CreateForm(forms.ModelForm):
    title = forms.CharField(required=True, label='Название')

    def __init__(self, *args, **kwargs):
        super(CreateForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs = {
            'class': 'form-control'
        }

    class Meta:
        model = Departments
        fields = '__all__'
