from django.urls import path
from . import views

urlpatterns = [
    path('', views.DepartmentsList.as_view(), name='departments_list'),
    path('new/', views.DepartmentsCreate.as_view(), name='departments_new'),
    path('edit/<int:pk>/', views.DepartmentsUpdate.as_view(), name='departments_edit'),
    path('delete/<int:pk>/', views.DepartmentsDelete.as_view(), name='departments_delete'),
]
