from django.urls import path, include

urlpatterns = [
    path('', include('main.frontend.urls'), name='main'),
    path('employees/', include('employees.frontend.urls'), name='employees'),
    path('departments/', include('departments.frontend.urls'), name='departments'),
    path('offices/', include('offices.frontend.urls'), name='offices'),
    path('accounts/', include('clients.frontend.urls'), name='accounts'),
    path('dashboard/', include('employees.dashboard.urls'), name='dashboard_employees'),
    path('dashboard/couples/', include('couples.dashboard.urls'), name='dashboard_couples'),
    path('dashboard/departments/', include('departments.dashboard.urls'), name='dashboard_departments'),
    path('dashboard/offices/', include('offices.dashboard.urls'), name='dashboard_offices'),
    path('dashboard/clients/', include('clients.dashboard.urls'), name='dashboard_clients'),
]

handler404 = 'main.frontend.views.handlerErrorNotFound'
handler500 = 'main.frontend.views.handlerErrorCode'
handler403 = 'main.frontend.views.handlerErrorAccess'
handler400 = 'main.frontend.views.handlerErrorRequest'
