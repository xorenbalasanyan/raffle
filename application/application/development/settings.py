from application.settings import *

DEBUG = True
ALLOWED_HOSTS = ['*']

ROOT_URLCONF = 'application.development.urls'
