from offices.models import Offices
from django import forms

class CreateForm(forms.ModelForm):
    title =  forms.CharField(required=True, label='Название')

    def __init__(self, *args, **kwargs):
        super(CreateForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs = {
            'class': 'form-control'
        }

    class Meta:
        model = Offices
        fields = '__all__'
