from django.urls import path
from . import views

urlpatterns = [
    path('', views.OfficesList.as_view(), name='offices_list'),
    path('new/', views.OfficesCreate.as_view(), name='offices_new'),
    path('edit/<int:pk>/', views.OfficesUpdate.as_view(), name='offices_edit'),
    path('delete/<int:pk>/', views.OfficesDelete.as_view(), name='offices_delete'),
]
