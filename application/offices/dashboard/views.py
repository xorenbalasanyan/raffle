from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from offices.models import Offices
from offices.dashboard.forms import CreateForm
from main.components import SuperuserRequiredComponent

class OfficesList(SuperuserRequiredComponent, ListView):
    template_name = 'dashboard/offices/list.html.j2'
    model = Offices
    paginate_by = 30
    ordering = ['-id']

class OfficesCreate(SuperuserRequiredComponent, CreateView):
    form_class = CreateForm
    template_name = 'dashboard/offices/form.html.j2'
    model = Offices
    success_url = reverse_lazy('offices_list')

class OfficesUpdate(SuperuserRequiredComponent, UpdateView):
    form_class = CreateForm
    template_name = 'dashboard/offices/form.html.j2'
    model = Offices
    success_url = reverse_lazy('offices_list')

class OfficesDelete(SuperuserRequiredComponent, DeleteView):
    template_name = 'dashboard/common/delete.html.j2'
    model = Offices
    success_url = reverse_lazy('offices_list')
