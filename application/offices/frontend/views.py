from offices.models import Offices
from django.http import JsonResponse

def autocomplete(request):
    list = []
    offices = Offices.objects.filter(title__icontains=request.GET.get('q')).all()
    for office in offices:
        list.append(office.title)

    return JsonResponse({'list': list})
