from django.db import models
from offices.models import Offices
from departments.models import Departments

class Employees(models.Model):

    STATUS_UNREGISTERED = 0
    STATUS_REGISTERED   = 1

    STATUS_CHOICES = (
        (STATUS_REGISTERED, 'Зарегистрирован'),
        (STATUS_UNREGISTERED, 'Не прошел регистрацию'),
    )

    first_name = models.CharField(max_length=255, default=None, null=True, blank=True)
    last_name  = models.CharField(max_length=255, default=None, null=True, blank=True)
    sur_name   = models.CharField(max_length=255, default=None, null=True, blank=True)
    email      = models.CharField(max_length=255, default=None, null=True, blank=True)
    department = models.ForeignKey(Departments, default=None, null=True, on_delete=models.SET_NULL, blank=True)
    office     = models.ForeignKey(Offices, default=None, null=True, on_delete=models.SET_NULL, blank=True)
    giver      = models.ForeignKey('self', default=None, null=True, blank=True, on_delete=models.SET_NULL, related_name='+')
    status     = models.SmallIntegerField(choices=STATUS_CHOICES, default=STATUS_UNREGISTERED, null=True, blank=True)

    class Meta:
        db_table = "t_employees"
