from django import template
from employees.frontend.forms import SubscribeForm

register = template.Library()

@register.simple_tag
def getSubscribeForm():
    form = SubscribeForm()
    return form
