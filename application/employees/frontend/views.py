from employees.models import Employees
from departments.models import Departments
from offices.models import Offices
from django.http import JsonResponse, Http404, HttpResponseRedirect
from employees.frontend.forms import SubscribeForm
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.conf import settings
import re
import basehash
from django.shortcuts import get_object_or_404
from django.urls import reverse

def subscribe(request):
    if request.method == 'POST' and request.is_ajax():
        data            = {}
        data['success'] = False
        data['message'] = 'Произошла ошибка! Возможно Вы указали неправильные данные.'

        form = SubscribeForm(request.POST)
        if form.is_valid():
            full_name  = re.sub('[,]', '', form.cleaned_data.get('name'))
            full_name  = full_name.strip().split()

            if len(full_name) == 3:
                employee   = True
                department = True
                office     = True

                try:
                    department = Departments.objects.get(title=form.cleaned_data['department'])
                except:
                    department = False

                try:
                    office = Offices.objects.get(title=form.cleaned_data['office'])
                except:
                    office = False

                if department and office:
                    try:
                        employee = Employees.objects.get(
                            first_name = full_name[1],
                            last_name  = full_name[0],
                            sur_name   = full_name[2],
                            department = department,
                            office     = office,
                            email      = form.cleaned_data['email']
                        )
                    except:
                        employee = False

                    if employee:
                        if employee.status is not Employees.STATUS_REGISTERED:
                            msg = EmailMessage(
                                'Регистрация "Secret Santa"',
                                get_template('mail/subscribe.html.j2').render({
                                    'slug': basehash.base36().hash(employee.id),
                                    'request': request,
                                }),
                                to=[employee.email],
                                from_email=settings.EMAIL_HOST_USER
                            )
                            msg.content_subtype = 'html'

                            isSend = True
                            try:
                                msg.send()
                            except:
                                isSend = False

                            if isSend:
                                data['success'] = True
                                data['message'] = ''

        return JsonResponse(data)
    else:
        raise Http404

def autocomplete(request):
    list = []
    employees = Employees.objects.filter(last_name__icontains=request.GET.get('q'), status=Employees.STATUS_UNREGISTERED).all()
    for employee in employees:
        list.append(employee.last_name+' '+employee.first_name+' '+employee.sur_name)

    return JsonResponse({'list': list})

def confirm(request, slug):
    try:
        id = basehash.base36().unhash(slug)
    except:
        raise Http404
    queryset = Employees.objects.filter(pk=id, status=Employees.STATUS_UNREGISTERED)
    employee = get_object_or_404(queryset)

    employee.status = Employees.STATUS_REGISTERED
    employee.save()

    return HttpResponseRedirect(reverse('home')+'#subscribed')
