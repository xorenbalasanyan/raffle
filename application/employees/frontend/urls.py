from django.urls import path
from . import views

urlpatterns = [
    path('subscribe/', views.subscribe, name='employees_subscribe'),
    path('autocomplete/', views.autocomplete, name='employees_autocomplete'),
    path('confirm/<str:slug>/', views.confirm, name='employees_confirm'),
]
