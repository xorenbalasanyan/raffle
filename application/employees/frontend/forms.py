from employees.models import Employees
from django import forms
from django.urls import reverse

class SubscribeForm(forms.Form):
    name       = forms.CharField(required=True, label='Фамилия, Имя и Отчество')
    email      = forms.EmailField(required=True, label='E-mail:')
    department = forms.CharField(required=True, label='Департамент')
    office     = forms.CharField(required=True, label='Офис')

    def __init__(self, *args, **kwargs):
        super(SubscribeForm, self).__init__(*args, **kwargs)

        self.fields['name'].widget.attrs = {
            'class': 'form_input js-subscribe-name',
            'data-autocomplete': reverse('employees_autocomplete')
        }
        self.fields['email'].widget.attrs = {
            'class': 'form_input'
        }
        self.fields['department'].widget.attrs = {
            'class': 'form_input js-subscribe-department',
            'data-autocomplete': reverse('departments_autocomplete')
        }
        self.fields['office'].widget.attrs = {
            'class': 'form_input js-subscribe-office',
            'data-autocomplete': reverse('offices_autocomplete')
        }
