from employees.models import Employees
from django import forms
from django.forms import ModelChoiceField
from departments.models import Departments
from offices.models import Offices
from couples.models import Couples
from couples.dashboard.views import sendMail

class EmployeesModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.title

class CreateForm(forms.ModelForm):
    first_name = forms.CharField(required=True, label='Имя')
    last_name  = forms.CharField(required=True, label='Фамилия')
    sur_name   = forms.CharField(required=True, label='Отчество')
    email      = forms.EmailField(required=True, label='Емейл')
    giver      = forms.CharField(required=False, label='Даритель')
    department = EmployeesModelChoiceField(required=True, label='Департамент', queryset=Departments.objects.all())
    office     = EmployeesModelChoiceField(required=True, label='Офис', queryset=Offices.objects.all())
    status     = forms.ChoiceField(choices=Employees.STATUS_CHOICES, label='Статус', widget=forms.Select(), required=True)

    def __init__(self, *args, **kwargs):
        self.old_giver = False
        if kwargs.get('instance'):
            initial = kwargs.setdefault('initial', {})
            if kwargs['instance'].giver:
                self.old_giver   = kwargs['instance'].giver
                initial['giver'] = kwargs['instance'].giver.first_name+' '+kwargs['instance'].giver.last_name+' '+kwargs['instance'].giver.sur_name

        super(CreateForm, self).__init__(*args, **kwargs)

        self.fields['first_name'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['last_name'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['sur_name'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['email'].widget.attrs = {
            'class': 'form-control js-email'
        }
        self.fields['department'].widget.attrs = {
            'class': 'form-control js-department'
        }
        self.fields['office'].widget.attrs = {
            'class': 'form-control js-office'
        }
        self.fields['status'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['giver'].widget.attrs = {
            'class': 'form-control js-giver'
        }

    def clean_giver(self):
        content = self.cleaned_data['giver']
        if content is '':
            content = None
        else:
            full_name = content.strip().split()

            try:
                content = Employees.objects.get(
                    first_name = full_name[0],
                    last_name  = full_name[1],
                    sur_name   = full_name[2],
                    department = self.cleaned_data['department'],
                    office     = self.cleaned_data['office']
                )
            except:
                raise forms.ValidationError('Нет такого сотрудника на данном департаменте/офисе!')

        return content

    def save(self):
        old_giver = self.old_giver
        giver     = self.cleaned_data['giver']
        instance  = forms.ModelForm.save(self)

        coupleControll(giver, old_giver, instance)

        return instance

    class Meta:
        model = Employees
        fields = '__all__'

def coupleControll(giver, old_giver, instance):
    if giver and giver != old_giver:
        isGiverBusy = True
        try:
            isGiverBusy = Employees.objects.exclude(id=instance.id).get(giver=giver)
        except:
            isGiverBusy = False

        if isGiverBusy:
            isGiverBusy.giver = None
            isGiverBusy.save()

        couple = Couples.objects.create(sender=giver, taker=instance)
        sendMail(giver, instance)

class FilterForm(forms.Form):
    TYPE_FILTER = 1
    TYPE_EXPORT = 2
    TYPES = (
        (TYPE_FILTER, 'Filter'),
        (TYPE_EXPORT, 'Export')
    )
    status = forms.ChoiceField(choices=(('', 'Все'),)+Employees.STATUS_CHOICES, label='Статус', widget=forms.Select(), required=False)
    type   = forms.ChoiceField(choices=TYPES, label='Type', widget=forms.Select(), required=False)

    first_name = forms.CharField(required=False, label='Имя')
    last_name  = forms.CharField(required=False, label='Фамилия')
    sur_name   = forms.CharField(required=False, label='Отчество')
    email      = forms.EmailField(required=False, label='Емейл')
    department = EmployeesModelChoiceField(required=False, label='Департамент', queryset=Departments.objects.all())
    office     = EmployeesModelChoiceField(required=False, label='Офис', queryset=Offices.objects.all())

    def __init__(self, *args, **kwargs):
        super(FilterForm, self).__init__(*args, **kwargs)
        self.fields['status'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['first_name'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['last_name'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['sur_name'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['email'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['department'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['office'].widget.attrs = {
            'class': 'form-control'
        }

class ImportForm(forms.Form):
    file = forms.FileField(label='XLSX файл для импорта', widget=forms.FileInput(attrs={'multiple': False}), required=False)
