from django.urls import path
from . import views

urlpatterns = [
    path('', views.EmployeesList.as_view(), name='employees_list'),
    path('new/', views.EmployeesCreate.as_view(), name='employees_new'),
    path('edit/<int:pk>/', views.EmployeesUpdate.as_view(), name='employees_edit'),
    path('delete/<int:pk>/', views.EmployeesDelete.as_view(), name='employees_delete'),
    path('employees_givers/', views.employees_givers, name='employees_givers'),
    path('import/', views.importFile, name='employees_import'),
    path('registration/', views.employeeRegistration, name='employees_registration'),
]
