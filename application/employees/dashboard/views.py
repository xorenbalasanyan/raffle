from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from employees.models import Employees
from employees.dashboard.forms import CreateForm, FilterForm, ImportForm
from main.components import SuperuserRequiredComponent


from django.http import JsonResponse, Http404, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import user_passes_test

import openpyxl
from openpyxl import Workbook
from openpyxl.styles import PatternFill
from django.http import HttpResponse

from offices.models import Offices
from departments.models import Departments

@user_passes_test(lambda u: u.is_superuser)
def employees_givers(request):
    list = []
    givers = Employees.objects.filter(
            first_name__icontains=request.GET.get('q'),
            department=request.GET.get('department'),
            office=request.GET.get('office'),
            status=Employees.STATUS_REGISTERED
        ).exclude(
            email=request.GET.get('email')
        ).all()

    for giver in givers:
        list.append(giver.first_name+' '+giver.last_name+' '+giver.sur_name)

    return JsonResponse({'list': list})

class EmployeesList(SuperuserRequiredComponent, ListView):
    template_name = 'dashboard/employees/list.html.j2'
    model         = Employees
    paginate_by   = 30
    ordering      = ['-id']

    def get(self, *args, **kwargs):
        form = FilterForm(self.request.GET)
        if form.is_valid():
            if form.cleaned_data.get('type') and int(form.cleaned_data.get('type')) == FilterForm.TYPE_EXPORT:
                options = GenerateQuery(form)
                return export(Employees.objects.filter(**options).all())
        return super(EmployeesList, self).get(*args, **kwargs)


    def get_context_data(self, **kwargs):
        ctx                = super(EmployeesList, self).get_context_data(**kwargs)
        ctx['filter_form'] = FilterForm(self.request.GET)
        ctx['import_form'] = ImportForm
        return ctx
    def get_queryset(self):
        qs      = super().get_queryset()
        options = {}
        form    = FilterForm(self.request.GET)

        if form.is_valid():
            options = GenerateQuery(form)

        return qs.filter(**options)

def GenerateQuery(form):
    options    = {}
    status     = form.cleaned_data.get('status')
    office     = form.cleaned_data.get('office')
    department = form.cleaned_data.get('department')
    first_name = form.cleaned_data.get('first_name')
    last_name  = form.cleaned_data.get('last_name')
    sur_name   = form.cleaned_data.get('sur_name')
    email      = form.cleaned_data.get('email')

    if status:
        options['status'] = int(status)
    if office:
        options['office'] = int(office.id)
    if department:
        options['department'] = int(department.id)
    if first_name:
        options['first_name__icontains'] = str(first_name)
    if last_name:
        options['last_name__icontains'] = str(last_name)
    if sur_name:
        options['sur_name__icontains'] = str(sur_name)
    if email:
        options['email__icontains'] = str(email)
    return options

class EmployeesCreate(SuperuserRequiredComponent, CreateView):
    form_class = CreateForm
    template_name = 'dashboard/employees/form.html.j2'
    model = Employees
    success_url = reverse_lazy('employees_list')

class EmployeesUpdate(SuperuserRequiredComponent, UpdateView):
    form_class = CreateForm
    template_name = 'dashboard/employees/form.html.j2'
    model = Employees
    success_url = reverse_lazy('employees_list')

class EmployeesDelete(SuperuserRequiredComponent, DeleteView):
    template_name = 'dashboard/common/delete.html.j2'
    model = Employees
    success_url = reverse_lazy('employees_list')

@user_passes_test(lambda u: u.is_superuser)
def employeeRegistration(request):
    employee        = True
    data            = {}
    data['success'] = False
    if request.method == 'POST':
        id = request.POST.get('employee', 0)
        try:
            employee = Employees.objects.get(pk=id, status=Employees.STATUS_UNREGISTERED)
        except:
            employee = False
        if employee:
            employee.status = Employees.STATUS_REGISTERED
            employee.save()
            data['success'] = True

    return JsonResponse(data)

def export(employees):
    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )
    response['Content-Disposition'] = 'attachment; filename=base.xls'
    workbook        = Workbook()
    worksheet       = workbook.active
    worksheet.title = 'Экспорт сотрудников'

    columns = [
        'ФИО',
        'Емайл',
        'Офис',
        'Департамент',
        'Даритель',
    ]
    row_num = 1

    worksheet.column_dimensions['A'].width = 35
    worksheet.column_dimensions['B'].width = 35
    worksheet.column_dimensions['C'].width = 35
    worksheet.column_dimensions['D'].width = 35
    worksheet.column_dimensions['E'].width = 35

    for col_num, column_title in enumerate(columns, 1):
        cell       = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title
        cell.fill  = PatternFill("solid", fgColor='F1C232')

    for employee in employees:
        row_num += 1
        couple   = employee.taker_employee.first()
        giver    = ''
        if couple:
            giver = couple.sender.last_name + ' ' + couple.sender.first_name + ' ' + couple.sender.sur_name

        row = [
            employee.last_name + ' ' + employee.first_name + ' ' + employee.sur_name,
            employee.email,
            employee.office.title,
            employee.department.title,
            giver,
        ]

        for employe_num, cell_value in enumerate(row, 1):
            cell       = worksheet.cell(row=row_num, column=employe_num)
            cell.value = cell_value

    workbook.save(response)

    return response


@user_passes_test(lambda u: u.is_superuser)
def importFile(request):
    if "POST" == request.method:
        form = ImportForm(request.POST, request.FILES)

        if form.is_valid():
            file = form.cleaned_data.get('file')
            if file:
                wb      = openpyxl.load_workbook(file)
                rows    = wb.active
                max_row = rows.max_row

                for i in range(1, max_row + 1):
                    full_name = str(rows.cell(row = i, column = 1).value).strip()
                    full_name = full_name.strip().split()

                    if len(full_name) == 3:
                        first_name                    = full_name[1]
                        last_name                     = full_name[0]
                        sur_name                      = full_name[2]
                        office, office_create         = Offices.objects.get_or_create(title=str(rows.cell(row = i, column = 2).value).strip())
                        department, department_create = Departments.objects.get_or_create(title=str(rows.cell(row = i, column = 3).value).strip())
                        email                         = str(rows.cell(row = i, column = 4).value).strip()
                        Employees.objects.create(
                            first_name = first_name,
                            last_name  = last_name,
                            sur_name   = sur_name,
                            office     = office,
                            department = department,
                            email      = email,
                        )
        return HttpResponseRedirect(reverse('employees_list'))
    raise Http404
