from django.core.management.base import BaseCommand
from employees.models import Employees
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import get_template

def reminder():
    employees = Employees.objects.filter(status=Employees.STATUS_UNREGISTERED).values('email')
    for employee in employees:
        if employee['email']:
            msg = EmailMessage(
                'Secret Santa',
                get_template('mail/reminder.html.j2').render(),
                to=[employee['email']],
                from_email=settings.EMAIL_HOST_USER
            )
            msg.content_subtype = 'html'

            try:
                msg.send()
            except:
                pass
    pass

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        reminder()
