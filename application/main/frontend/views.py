from django.shortcuts import render

def index(request):
    return render(request, 'frontend/main/index.html.j2', {})

def handlerErrorNotFound(request, exception=None):
    return render(request, 'frontend/main/error.html.j2', {
        'status_code':404,
        'title': 'Запрашиваемая страница не найдена',
        'text': 'Страница, которую вы ищете, не существует. Возможно, страница была перемещена или удалена.'
    },
    status = 404)

def handlerErrorAccess(request, exception=None):
    return render(request, 'frontend/main/error.html.j2', {
        'status_code':403,
        'title': 'У вас нет прав для просмотра этой страницы',
        'text': 'Зайдите на сайт под своим логином и паролем.'
    },
    status = 403)

def handlerErrorRequest(request, exception=None):
    return render(request, 'frontend/main/error.html.j2', {
        'status_code':400,
        'title': 'Что-то пошло не так',
        'text': 'Попробуйте повторить попытку позже.'
    },
    status = 400)

def handlerErrorCode(request, exception=None):
    return render(request, 'frontend/main/error.html.j2', {
        'status_code':500,
        'title': 'Что-то пошло не так',
        'text': 'Попробуйте повторить попытку позже.'
    },
    status = 500)
